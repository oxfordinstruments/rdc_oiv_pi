import logging
import yaml
import sqlite3
import os.path


class oivConfig(object):

    conn = None


    def __init__(self, log=None, debug=False, root=None):
        self.log = log or logging.getLogger(__name__)
        self.debug = debug
        self.root = root
        self.dbPath = '{}var/oivision.db3'.format(self.root)
        self.settingsPath = '{}var/.settings'.format(self.root)
        self.keyPath = '{}var/.key'.format(self.root)
        self.key = '7549oivision105685'
        self.logPath = '{}var/oivision.log'.format(self.root)

    def connectDb(self, reinit=False):
        if reinit:
            self.closeDb()
            try:
                self.closeDb()
                os.remove(self.dbPath)
            except Exception:
                return False

        if os.path.isfile(self.dbPath):
            if self.conn is None:
                conn = sqlite3.connect(self.dbPath)
                self.conn = conn
        else:
            self.log.info('Creating database')
            conn = sqlite3.connect(self.dbPath)
            self.conn = conn
            self.initDb()

        return self.conn

    def closeDb(self):
        if self.conn is not None:
            self.conn.close()

    def getConfig(self):
        if not os.path.isfile(self.settingsPath):
            self.log.error('Settings file is missing')
            raise Exception('Settings file is missing')
        with open(self.settingsPath) as fp:
            cfg = yaml.load(fp)
        if self.debug:
            print("Dump Config:")
            print(yaml.dump(cfg))
            cfg['errors'] = None

        return cfg

    def initDb(self):
        c = self.conn.cursor()
        self.log.info('Created `data` table')
        c.execute('''CREATE TABLE data (
                        id INTEGER PRIMARY KEY ,
                        mac TEXT NOT NULL,
                        tf NUMERIC NOT NULL,
                        rh NUMERIC NOT NULL,
                        dp NUMERIC NOT NULL,
                        dpd NUMERIC NOT NULL,
                        he NUMERIC NOT NULL,
                        wf NUMERIC NOT NULL,
                        wt NUMERIC NOT NULL,
                        timestamp TEXT NOT NULL,
                        vp NUMERIC NOT NULL,
                        q INTEGER NOT NULL,
                        cmp INTEGER NOT NULL,
                        in_storage INTEGER NOT NULL,
                        has_alarm INTEGER NOT NULL,
                        has_critical_alarm INTEGER NOT NULL,
                        tfa INTEGER NOT NULL,
                        rha INTEGER NOT NULL,
                        dpa INTEGER NOT NULL,
                        dpda INTEGER NOT NULL,
                        hea INTEGER NOT NULL,
                        vpa INTEGER NOT NULL,
                        wfa INTEGER NOT NULL,
                        wta INTEGER NOT NULL,
                        tfc INTEGER NOT NULL,
                        rhc INTEGER NOT NULL,
                        dpc INTEGER NOT NULL,
                        dpdc INTEGER NOT NULL,
                        hec INTEGER NOT NULL,
                        vpc INTEGER NOT NULL,
                        wfc INTEGER NOT NULL,
                        wtc INTEGER NOT NULL,
                        qc INTEGER NOT NULL,
                        cmpc INTEGER NOT NULL,
                        uploaded INTEGER NOT NULL);''')
        self.conn.commit()

        self.log.info('Created `alarmSettings` table')
        c.execute('''CREATE TABLE `alarmSettings` (
                        `id` INTEGER PRIMARY KEY,
                        `monitor_he`	INTEGER NOT NULL,
                        `monitor_heb`	INTEGER,
                        `monitor_vp`	INTEGER NOT NULL,
                        `monitor_tf`	INTEGER NOT NULL,
                        `monitor_rh`	INTEGER NOT NULL,
                        `monitor_dp`	INTEGER NOT NULL,
                        `monitor_dpd`	INTEGER NOT NULL,
                        `monitor_wf`	INTEGER NOT NULL,
                        `monitor_wt`	INTEGER NOT NULL,
                        `monitor_q`	    INTEGER NOT NULL,
                        `monitor_cmp`	INTEGER NOT NULL,
                        `in_storage`	INTEGER NOT NULL,
                        `avg_he`	    INTEGER NOT NULL,
                        `avg_vp`	    INTEGER NOT NULL,
                        `avg_cnt`	    INTEGER NOT NULL,
                        `alarm_he_l`	NUMERIC NOT NULL,
                        `alarm_heb_h`	NUMERIC,
                        `alarm_vp_h`	NUMERIC NOT NULL,
                        `alarm_vp_l`	NUMERIC NOT NULL,
                        `alarm_tf_h`	NUMERIC NOT NULL,
                        `alarm_tf_l`	NUMERIC NOT NULL,
                        `alarm_rh_h`	NUMERIC NOT NULL,
                        `alarm_rh_l`	NUMERIC NOT NULL,
                        `alarm_dp_h`	NUMERIC NOT NULL,
                        `alarm_dp_diff`	NUMERIC NOT NULL,
                        `alarm_wf_h`	NUMERIC NOT NULL,
                        `alarm_wf_l`	NUMERIC NOT NULL,
                        `alarm_wt_h`	NUMERIC NOT NULL,
                        `alarm_wt_l`	NUMERIC NOT NULL,
                        `alarm_storage_he_l`	NUMERIC NOT NULL,
                        `alarm_storage_vp_h`	NUMERIC NOT NULL,
                        `alarm_storage_vp_l`	NUMERIC NOT NULL,
                        `critical_he_l`	NUMERIC NOT NULL,
                        `critical_heb_h`	NUMERIC,
                        `critical_vp_h`	NUMERIC NOT NULL,
                        `critical_vp_l`	NUMERIC NOT NULL,
                        `critical_tf_h`	NUMERIC NOT NULL,
                        `critical_tf_l`	NUMERIC NOT NULL,
                        `critical_rh_h`	NUMERIC NOT NULL,
                        `critical_rh_l`	NUMERIC NOT NULL,
                        `critical_dp_h`	NUMERIC NOT NULL,
                        `critical_dp_diff`  NUMERIC NOT NULL,
                        `critical_wf_h`	NUMERIC NOT NULL,
                        `critical_wf_l`	NUMERIC NOT NULL,
                        `critical_wt_h`	NUMERIC NOT NULL,
                        `critical_wt_l`	NUMERIC NOT NULL,
                        `critical_storage_he_l`	NUMERIC NOT NULL,
                        `critical_storage_vp_h`	NUMERIC NOT NULL,
                        `critical_storage_vp_l`	NUMERIC NOT NULL,
                        `offset_he`	NUMERIC NOT NULL,
                        `offset_vp`	NUMERIC NOT NULL,
                        `offset_wf`	NUMERIC NOT NULL,
                        `offset_wtf` NUMERIC NOT NULL,
                        `offset_tc`	NUMERIC NOT NULL,
                        `offset_rh`	NUMERIC NOT NULL,
                        `updated`	TEXT,
                        `timestamp`	DATETIME DEFAULT CURRENT_TIMESTAMP);''')
        self.conn.commit()

        self.log.info('Created `migrations` table')
        c.execute('''CREATE TABLE migrations (
                        id INTEGER PRIMARY KEY,
                        applied INTEGER NOT NULL,
                        migration_file TEXT NOT NULL);
        ''')
        self.conn.commit()

        self.log.info('Created `server` table')
        c.execute('''CREATE TABLE server (
                        id INTEGER PRIMARY KEY,
                        last_host TEXT NOT NULL,
                        `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP);''')
        self.conn.commit()

        self.log.info('Created `oiv` table')
        c.execute('''CREATE TABLE oiv (
                        id INTEGER PRIMARY KEY,
                        last_oiv_mac TEXT NOT NULL,
                        `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP);''')
        self.conn.commit()

        self.log.info('Created `keys` table')
        c.execute('''CREATE TABLE keys (
                        id INTEGER PRIMARY KEY,
                        last_api_key TEXT NOT NULL,
                        `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP);''')
        self.conn.commit()

        self.log.info('Created `hellos` table')
        c.execute('''CREATE TABLE hellos (
                        id INTEGER PRIMARY KEY,
                        last_hello TEXT NOT NULL,
                        `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP);''')
        self.conn.commit()

        self.log.info('Created `uploads` table')
        c.execute('''CREATE TABLE uploads (
                        id INTEGER PRIMARY KEY,
                        last_upload TEXT NOT NULL,
                        `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP);''')
        self.conn.commit()

        self.log.info('Created `cleaning` table')
        c.execute('''CREATE TABLE cleaning (
                        id INTEGER PRIMARY KEY,
                        last_clean TEXT NOT NULL,
                        `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP);''')
        self.conn.commit()

        self.log.info('Database created')

    def setKey(self, key=None):
        if not os.path.isfile(self.keyPath) or key is None:
            key = self.key
            if self.conn is None:
                self.connectDb()
            c = self.conn.cursor()
            c.execute("DELETE FROM keys;")
            self.conn.commit()
        data = {'key': key}
        with open(self.keyPath, 'w') as fp:
            yaml.dump(data, fp, default_flow_style=False)
        if self.conn is None:
            self.connectDb()
        c = self.conn.cursor()
        c.execute("INSERT INTO keys (last_api_key) VALUES ('{}');".format(key))
        self.conn.commit()
        self.log.info('Set API Key to: {}'.format(key))

    def getKey(self):
        if not os.path.isfile(self.keyPath):
              self.setKey()
        with open(self.keyPath, 'r') as fp:
            data = yaml.load(fp)
        return data['key']

    def reset(self):
        try:
            os.remove(self.dbPath)
        except FileNotFoundError or PermissionError:
            pass

        try:
            os.remove(self.keyPath)
        except FileNotFoundError or PermissionError:
            pass

        try:
            os.remove(self.logPath)
        except Exception:
            pass
