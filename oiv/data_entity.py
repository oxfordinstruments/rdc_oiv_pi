class dataEntity(object):

    def __init__(self):
        self._rh = None
        self._rha = False
        self._rhc = False

        self._dp = None
        self._dpa = False
        self._dpc = False

        self._dpd = None
        self._dpda = False
        self._dpdc = False

        self._tf = None
        self._tfa = False
        self._tfc = False

        self._he = None
        self._hea = False
        self._hec = False

        self._wf = None
        self._wfa = False
        self._wfc = False

        self._wt = None
        self._wta = False
        self._wtc = False

        self._vp = None
        self._vpa = False
        self._vpc = False

        self._q = None
        self._qc = False

        self._cmp = None
        self._cmpc = False

        self._mac = None
        self._in_storage = False
        self._has_alarm = False
        self._has_critical_alarm = False
        self._uploaded = False

    @property
    def mac(self): return self._mac
    @mac.setter
    def mac(self, mac): self._mac = mac

    #
    # TF
    #
    @property
    def tf(self): return self._tf
    @tf.setter
    def tf(self, tf): self._tf = tf

    @property
    def tfa(self): return self._tfa
    @tfa.setter
    def tfa(self, tfa): self._tfa = tfa

    @property
    def tfc(self): return self._tfc
    @tfc.setter
    def tfc(self, tfc): self._tfc = tfc

    #
    # RH
    #
    @property
    def rh(self): return self._rh
    @rh.setter
    def rh(self, rh): self._rh = rh

    @property
    def rha(self): return self._rha
    @rha.setter
    def rha(self, rha): self._rha = rha

    @property
    def rhc(self): return self._rhc
    @rhc.setter
    def rhc(self, rhc): self._rhc = rhc

    #
    # DP
    #
    @property
    def dp(self): return self._dp

    @dp.setter
    def dp(self, dp): self._dp = dp

    @property
    def dpa(self): return self._dpa

    @dpa.setter
    def dpa(self, dpa): self._dpa = dpa

    @property
    def dpc(self): return self._dpc

    @dpc.setter
    def dpc(self, dpc): self._dpc = dpc

    #
    # DPD
    #
    @property
    def dpd(self): return self._dpd

    @dpd.setter
    def dpd(self, dpd): self._dpd = dpd

    @property
    def dpda(self): return self._dpda

    @dpda.setter
    def dpda(self, dpda): self._dpda = dpda

    @property
    def dpdc(self): return self._dpdc

    @dpdc.setter
    def dpdc(self, dpdc): self._dpdc = dpdc

    #
    # HE
    #
    @property
    def he(self): return self._he
    @he.setter
    def he(self, he): self._he = he

    @property
    def hea(self): return self._hea
    @hea.setter
    def hea(self, hea): self._hea = hea

    @property
    def hec(self): return self._hec
    @hec.setter
    def hec(self, hec): self._hec = hec

    #
    # WF
    #
    @property
    def wf(self): return self._wf
    @wf.setter
    def wf(self, wf): self._wf = wf

    @property
    def wfa(self): return self._wfa
    @wfa.setter
    def wfa(self, wfa): self._wfa = wfa

    @property
    def wfc(self): return self._wfc
    @wfc.setter
    def wfc(self, wfc): self._wfc = wfc

    #
    # WT
    #
    @property
    def wt(self): return self._wt
    @wt.setter
    def wt(self, wt): self._wt = wt

    @property
    def wta(self): return self._wta
    @wta.setter
    def wta(self, wta): self._wta = wta

    @property
    def wtc(self): return self._wtc
    @wtc.setter
    def wtc(self, wtc): self._wtc = wtc


    #
    # VP
    #
    @property
    def vp(self): return self._vp
    @vp.setter
    def vp(self, vp): self._vp = vp

    @property
    def vpa(self): return self._vpa
    @vpa.setter
    def vpa(self, vpa): self._vpa = vpa

    @property
    def vpc(self): return self._vpc
    @vpc.setter
    def vpc(self, vpc): self._vpc = vpc

    #
    # Q (field)
    #
    @property
    def q(self): return self._q
    @q.setter
    def q(self, q): self._q = q

    @property
    def qc(self): return self._qc
    @qc.setter
    def qc(self, qc): self._qc = qc

    #
    # CMP
    #
    @property
    def cmp(self): return self._cmp
    @cmp.setter
    def cmp(self, cmp): self._cmp = cmp

    @property
    def cmpc(self): return self._cmpc
    @cmpc.setter
    def cmpc(self, cmpc): self._cmpc = cmpc

    #
    #
    #
    @property
    def in_storage(self): return self._in_storage
    @in_storage.setter
    def in_storage(self, in_storage): self._in_storage = in_storage

    @property
    def has_alarm(self): return self._has_alarm
    @has_alarm.setter
    def has_alarm(self, has_alarm): self._has_alarm = has_alarm

    @property
    def has_critical_alarm(self): return self._has_critical_alarm
    @has_critical_alarm.setter
    def has_critical_alarm(self, has_critical_alarm): self._has_critical_alarm = has_critical_alarm

    @property
    def uploaded(self): return self._uploaded
    @uploaded.setter
    def uploaded(self, uploaded): self._uploaded = uploaded


