from serial.tools import list_ports
import logging
from pprint import pprint
import serial, json
import sqlite3
import requests
from oiv.data_entity import dataEntity
from oiv.config import oivConfig
from datetime import datetime, timezone, timedelta
import urllib3

class oivUtils(object):

    _ser = None
    def __init__(self, sqliteconn, log=None, debug=False, config=None):
        self.config = config
        self.conn = sqliteconn
        self.log = log or logging.getLogger(__name__)
        self.debug = debug

    def goodServer(self, server: str):
        try:
            c = self.conn.cursor()
            c.execute('''INSERT INTO server (last_host) VALUES ('{}')'''.format(server))
            self.conn.commit()
        except Exception as e:
            self.log.error(e)

    def goodHello(self, data: str):
        try:
            c = self.conn.cursor()
            c.execute('''INSERT INTO hellos (last_hello) VALUES ('{}')'''.format(data))
            self.conn.commit()
        except Exception as e:
            self.log.error(e)

    def goodUpload(self):
        try:
            c = self.conn.cursor()
            c.execute('''INSERT INTO uploads (last_upload) VALUES ('{}')'''.format(datetime.now()))
            self.conn.commit()

        except Exception as e:
            self.log.error(e)

    def goodClean(self):
        try:
            c = self.conn.cursor()
            c.execute('''INSERT INTO cleaning (last_clean) VALUES ('{}')'''.format(datetime.now()))
            self.conn.commit()
        except Exception as e:
            self.log.error(e)

    def saveAlarmSettings(self):
        try:
            d = self.config['alarmSettings']
            c = self.conn.cursor()

            c.execute("SELECT id FROM alarmSettings WHERE updated = '{}'".format(d['updated']))
            t = c.fetchone()

            if t is not None:
                return True

            self.log.info('Saving alarmSettings to db')

            c.execute('''INSERT INTO alarmSettings (
                monitor_he,
                monitor_vp, 
                monitor_tf, 
                monitor_rh,
                monitor_dp,
                monitor_dpd, 
                monitor_wf, 
                monitor_wt, 
                monitor_q, 
                monitor_cmp, 
                in_storage, 
                avg_he,
                avg_vp,
                avg_cnt,
                alarm_he_l, 
                alarm_vp_h, 
                alarm_vp_l, 
                alarm_tf_h, 
                alarm_tf_l, 
                alarm_rh_h, 
                alarm_rh_l,
                alarm_dp_h,
                alarm_dp_diff, 
                alarm_wf_h, 
                alarm_wf_l, 
                alarm_wt_h, 
                alarm_wt_l, 
                alarm_storage_he_l, 
                alarm_storage_vp_h, 
                alarm_storage_vp_l, 
                critical_he_l, 
                critical_vp_h, 
                critical_vp_l, 
                critical_tf_h, 
                critical_tf_l, 
                critical_rh_h, 
                critical_rh_l,
                critical_dp_h,
                critical_dp_diff, 
                critical_wf_h, 
                critical_wf_l, 
                critical_wt_h, 
                critical_wt_l, 
                critical_storage_he_l, 
                critical_storage_vp_h, 
                critical_storage_vp_l, 
                offset_he, 
                offset_vp, 
                offset_wf, 
                offset_wtf, 
                offset_tc, 
                offset_rh, 
                updated) 
                         VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47},{48},{49},{50},{51},'{52}')'''.format(
                int(d['monitor_he']),
                int(d['monitor_vp']),
                int(d['monitor_tf']),
                int(d['monitor_rh']),
                int(d['monitor_dp']),
                int(d['monitor_dpd']),
                int(d['monitor_wf']),
                int(d['monitor_wt']),
                int(d['monitor_q']),
                int(d['monitor_cmp']),
                int(d['in_storage']),
                int(d['avg_he']),
                int(d['avg_vp']),
                d['avg_cnt'],
                d['alarm_he_l'],
                d['alarm_vp_h'],
                d['alarm_vp_l'],
                d['alarm_tf_h'],
                d['alarm_tf_l'],
                d['alarm_rh_h'],
                d['alarm_rh_l'],
                d['alarm_dp_h'],
                d['alarm_dp_diff'],
                d['alarm_wf_h'],
                d['alarm_wf_l'],
                d['alarm_wt_h'],
                d['alarm_wt_l'],
                d['alarm_storage_he_l'],
                d['alarm_storage_vp_h'],
                d['alarm_storage_vp_l'],
                d['critical_he_l'],
                d['critical_vp_h'],
                d['critical_vp_l'],
                d['critical_tf_h'],
                d['critical_tf_l'],
                d['critical_rh_h'],
                d['critical_rh_l'],
                d['critical_dp_h'],
                d['critical_dp_diff'],
                d['critical_wf_h'],
                d['critical_wf_l'],
                d['critical_wt_h'],
                d['critical_wt_l'],
                d['critical_storage_he_l'],
                d['critical_storage_vp_h'],
                d['critical_storage_vp_l'],
                d['offset_he'],
                d['offset_vp'],
                d['offset_wf'],
                d['offset_wtf'],
                d['offset_tc'],
                d['offset_rh'],
                d['updated']
            )
            )
            self.conn.commit()
        except Exception as e:
            self.log.error(e)
            return False

        self.setBoxOffsetData()
        return True

    def loadAlarmSettings(self):
        self.log.info('Loading alarmSettings from db')
        try:
            self.conn.row_factory = sqlite3.Row
            c = self.conn.cursor()
            c.execute('SELECT * FROM alarmSettings ORDER BY id DESC LIMIT 1;')
            t = c.fetchall() # type: sqlite3.Row
            self.config['alarmSettings'] = dict(t[0])
            # Fix the int vals back to bool
            for key, val in self.config['alarmSettings'].items():
                d = [
                    'monitor_he',
                    'monitor_heb',
                    'monitor_vp',
                    'monitor_tf',
                    'monitor_rh',
                    'monitor_dp',
                    'monitor_dpd',
                    'monitor_wf',
                    'monitor_wt',
                    'monitor_q',
                    'monitor_cmp',
                    'in_storage',
                    'avg_he',
                    'avg_vp'
                ]
                if key in d:
                    self.config['alarmSettings'][key] = bool(val)
            if self.debug:
                pprint(self.config['alarmSettings'])

        except TypeError as te:
            self.log.error("1 Get alarmSettings error: {}".format(te))
            self.config['errors'][datetime.now().strftime(self.config['oiv_dt'])] = "1 RDC Get alarmSettings error: {}".format(te)
        except sqlite3.DatabaseError as de:
            self.log.error("2 Get alarmSettings error: {}".format(de))
            self.config['errors'][datetime.now().strftime(self.config['oiv_dt'])] = "2 RDC Get alarmSettings error: {}".format(de)
        except IndexError as de:
            self.log.error("3 Get alarmSettings error: {}".format(de))

    def getSerialPorts(self):
        _ports = list(list_ports.comports())

        # return the port if 'USB' is in the description
        for port_no, description, address in _ports:
            if '0403:6001' in address:
                return port_no
            else:
                return None

    def startSerial(self, port='/dev/ttyUSB0', baud=57600):
        if self._ser is None:
            ser = serial.Serial(port=port, baudrate=baud)
            self._ser = ser
            self.config['ser'] = ser
        return self._ser

    def stopSerial(self):
        if self.isBoxConnected(True) is False:
            return False
        if self._ser is not None:
            self._ser.close()
            self._ser = None

    def flushSerial(self):
        self._ser.flushInput()
        self._ser.flushOutput()

    def getJson(self, cmd: str):
        if self._ser is None:
            return False
        if self.isBoxConnected(True) is False:
            return False
        self.flushSerial()
        _cmd = "{}\n".format(cmd).encode()
        self._ser.write(_cmd)
        lines = self._ser.readlines(5)
        for line in lines:
            # print("DEBUG: " + line.decode())
            if line.decode("utf-8").find('{') >= 0:
                try:
                    js = json.loads(line.decode("utf-8"))
                    return js
                except Exception as e:
                    self.log.error(e)
                    if self.debug:
                        print(e)
        return False

    def sendJson(self, cmd:str, data: dict):
        j = json.dumps(data)
        # print("DEBUG: " + j)

        if self._ser is None:
            return False
        if self.isBoxConnected(True) is False:
            return False
        _cmd = "{0}\n{1}\n".format(cmd,j).encode()
        self._ser.write(_cmd)

        lines = self._ser.readlines(5)
        for line in lines:
            # print("DEBUG: " + line.decode())
            if line.decode("utf-8").find('*') >= 0:
                return True
        return False

    def sendCmd(self, cmd: str):
        if self._ser is None:
            return False
        if self.isBoxConnected(True) is False:
            return False
        _cmd = "{}\n".format(cmd).encode()
        self._ser.write(_cmd)

        lines = self._ser.readlines(5)
        for line in lines:
            # print("DEBUG: " + line.decode())
            if line.decode("utf-8").find('*') >= 0:
                return True
        return False

    def isBoxConnected(self, quick=False):
        if self._ser is None:
            self.log.error("Serial is none")
            return False
        if self._ser.getCTS() is True:
            return False
        elif quick:
            self.flushSerial()
            return True
        self._ser.write("{}\n".format('b').encode())
        lines = self._ser.readlines(1)
        for line in lines:
            # print("DEBUG: " + line.decode)
            if line.decode("utf-8") != "":
                return True
        return False

    def getOivInfo(self, conn: sqlite3.connect):
        bj = self.getJson('b')
        bj['mac'] = bj['mac'].lower()
        oivmac = bj['mac']
        c = conn.cursor()
        c.execute('SELECT last_oiv_mac FROM oiv ORDER BY id DESC LIMIT 1;')
        t = c.fetchone()
        # pprint(oivmac)
        # pprint(t[0])
        if t is not None:
            if t[0].lower() != oivmac:
                c = self.conn.cursor()
                c.execute('''INSERT INTO oiv (last_oiv_mac) VALUES ('{}')'''.format(oivmac))
                self.conn.commit()
        else:
            c = self.conn.cursor()
            c.execute('''INSERT INTO oiv (last_oiv_mac) VALUES ('{}')'''.format(oivmac))
            self.conn.commit()
        return bj

    def getRdcInfo(self, eth='eth0'):
        try:
            mac = open('/sys/class/net/{}/address'.format(eth)).readline()
        except:
            mac = "00:00:00:00:00:00"

        return mac[0:17].replace(':','-')

    def sayHello(self):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        self.log.info("Saying HELLO")
        js = None
        for server in self.config['server']:
            if self.debug:
                print("Trying Server: {}".format(server))
            try:
                headers = {'Content-Type':'application/json',
                           'api-key':self.config['key']}
                data = {'macrdc':self.config['mac_rdc'],
                        'macoiv':self.config['mac_oiv'],
                        'rev':self.config['box']['revision'],
                        'ver':self.config['box']['version'],
                        'errors':self.config['errors']}
                if self.debug:
                    print("Data to send for hello:")
                    pprint(data)
                req = requests
                res = req.request('POST', "{}/api/hello".format(server), verify=False, headers=headers, allow_redirects=True, data=json.dumps(data))
                if self.debug:
                    print("Request Response:")
                    pprint(res.text)
                try:
                    js = json.loads(res.text)
                    self.goodServer(server=server)
                    if js['code'] is not 202:
                        self.log.error('API code: {0} Error: {1}'.format(js['code'], js['error']))
                        return False
                except ValueError as ve:
                    self.log.error('Say Hello Error: {}'.format(ve))
                    return False
                break
            except requests.ConnectionError as ce:
                pprint(ce)
                continue

        self.config['hello'] = js
        self.goodHello(res.text)
        self.config['alarmSettings'] = self.config['hello']['settings']
        self.saveAlarmSettings()
        self.setBoxSiteInfo()
        self.setBoxMonitorInfo()
        if 'new_token' in self.config['hello']:
            cfg = oivConfig()
            cfg.setKey(self.config['hello']['new_token'])

        return True

    def setBoxSiteInfo(self, upload=False):
        if self._ser is None:
            return False
        if self.isBoxConnected(True) is False:
            return False
        if upload:
            _cmd1 = "&l1\nSystem ID: {}\n".format(self.config['upload']['sysid']).encode()
            name = self.config['upload']['name']
        else:
            _cmd1 = "&l1\nSystem ID: {}\n".format(self.config['hello']['sysid']).encode()
            name = self.config['hello']['name']
        self._ser.write(_cmd1)
        self.flushSerial()

        name = name[:19]
        _cmd2 = "&l2\n{}\n".format(name).encode()
        self._ser.write(_cmd2)
        self.flushSerial()

        if upload:
            _cmd1 = "&u\n{}\n".format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')).encode()
            self._ser.write(_cmd1)
            self.flushSerial()

    def setBoxMonitorInfo(self):
        if self._ser is None:
            return False
        if self.isBoxConnected(True) is False:
            return False

        als = self.config['alarmSettings']
        data = {
            'he': als['monitor_he'],
            'vp': als['monitor_vp'],
            'tf': als['monitor_tf'],
            'rh': als['monitor_rh'],
            'dp': als['monitor_dp'],
            'dpd': als['monitor_dpd'],
            'wf': als['monitor_wf'],
            'wt': als['monitor_wt'],
            'q': als['monitor_q'],
            'cmp': als['monitor_cmp']
        }
        self.sendJson('&m', data)

    def setBoxOffsetData(self):
        if self._ser is None:
            return False
        if self.isBoxConnected(True) is False:
            return False

        als = self.config['alarmSettings']
        data = {
            'he': float(als['offset_he']),
            'vp': float(als['offset_vp']),
            'tc': float(als['offset_tc']),
            'rh': float(als['offset_rh']),
            'wf': float(als['offset_wf']),
            'wtf': float(als['offset_wtf'])
        }
        self.sendJson('&o', data)

    def setBoxErrMsg(self, de: dataEntity):
        if self._ser is None:
            return False
        if self.isBoxConnected(True) is False:
            return False

        if de.has_alarm is False and de.has_critical_alarm is False:
            self.sendCmd('f')
            return True

        als = self.config['alarmSettings']

        c = 'C'
        a = 'A'
        n = ' '
        d = '-'

        he = n
        if als['monitor_he'] is False:
            he = d
        if de.hec:
            he = c
        elif de.hea:
            he = a

        vp = n
        if als['monitor_vp'] is False:
            vp = d
        if de.vpc:
            vp = c
        elif de.vpa:
            vp = a

        wf = n
        if als['monitor_wf'] is False:
            wf = d
        if de.wfc:
            wf = c
        elif de.wfa:
            wf = a

        wt = n
        if als['monitor_wt'] is False:
            wt = d
        if de.wtc:
            wt = c
        elif de.wta:
            wt = a

        tf = n
        if als['monitor_tf'] is False:
            tf = d
        if de.tfc:
            tf = c
        elif de.tfa:
            tf = a

        rh = n
        if als['monitor_rh'] is False:
            rh = d
        if de.rhc:
            rh = c
        elif de.rha:
            rh = a

        dp = n
        if als['monitor_dp'] is False:
            dp = d
        if de.dpc or de.dpdc:
            dp = c
        elif de.dpa or de.dpda:
            dp = a

        q = n
        if als['monitor_q'] is False:
            q = d
        if de.qc:
            q = c

        cmp = n
        if als['monitor_cmp'] is False:
            cmp = d
        if de.cmpc:
            cmp = c

        a = "H P WF WT T H D Q C"
        b = "{0} {1} {2}  {3}  {4} {5} {6} {7} {8}".format(he,vp,wf,wt,tf,rh,dp,q,cmp)

        _cmd = "&e1\n{}\n".format(a).encode()
        self._ser.write(_cmd)
        self.flushSerial()
        _cmd = "&e2\n{}\n".format(b).encode()
        self._ser.write(_cmd)
        return True

    def checkData(self):
        als = self.config['alarmSettings']
        data = self.config['data']
        self.log.info('Checking data: {}'.format(data))
        try:
            de = dataEntity()
            de.he = data['he']
            de.vp = data['vp']
            de.tf = data['tf']
            de.rh = data['rh']
            de.dp = data['dp']
            de.dpd = data['dpd']
            de.wf = data['wf']
            de.wt = data['wtf']
            de.q = data['q']
            de.cmp = data['comp']
            de.mac = self.config['mac_oiv']

            if self.config['alarmSettings']['avg_he']:
                sql = "SELECT he FROM data WHERE he != {0} ORDER BY id DESC LIMIT {1};".format(self.config['he_noread'],
                                                                                               self.config[
                                                                                                   'alarmSettings'][
                                                                                                   'avg_cnt'] - 1)
                self.conn.row_factory = sqlite3.Row
                c = self.conn.cursor()
                c.execute(sql)
                t = c.fetchall()  # type: sqlite3.Row
                for val in t:
                    de.he += val['he']
                de.he = de.he / (len(t) + 1)
                self.log.info('Averaged Helium by {}'.format(len(t) + 1))

            if self.config['alarmSettings']['avg_vp']:
                sql = "SELECT vp FROM data WHERE vp > {0} ORDER BY id DESC LIMIT {1};".format(self.config['mins']['vp'],
                                                                                              self.config[
                                                                                                  'alarmSettings'][
                                                                                                  'avg_cnt'] - 1)
                self.conn.row_factory = sqlite3.Row
                c = self.conn.cursor()
                c.execute(sql)
                t = c.fetchall()  # type: sqlite3.Row
                for val in t:
                    de.vp += val['vp']
                de.vp = de.vp / (len(t) + 1)
                self.log.info('Averaged Vessel Pressure by {}'.format(len(t) + 1))

            if de.tf >= self.config['mins']['tf']:
                if als['monitor_tf']:
                    if als['critical_tf_h'] <= de.tf or de.tf <= als['critical_tf_l']:
                        de.tfc = True
                        de.has_critical_alarm = True
                    elif als['alarm_tf_h'] <= de.tf or de.tf <= als['alarm_tf_l']:
                        de.tfa = True
                        de.has_alarm = True

            if de.rh >= self.config['mins']['rh']:
                if als['monitor_rh']:
                    if als['critical_rh_h'] <= de.rh or de.rh <= als['critical_rh_l']:
                        de.rhc = True
                        de.has_critical_alarm = True
                    elif als['alarm_rh_h'] <= de.rh or de.rh <= als['alarm_rh_l']:
                        de.rha = True
                        de.has_alarm = True

            if de.dp >= self.config['mins']['dp']:
                if als['monitor_dp']:
                    if als['critical_dp_h'] <= de.dp:
                        de.dpc = True
                        de.has_critical_alarm = True
                    elif als['alarm_dp_h'] <= de.dp:
                        de.dpa = True
                        de.has_alarm = True

            if de.dpd >= self.config['mins']['dpd']:
                if als['monitor_dpd']:
                    if de.dpd <= als['critical_dp_diff']:
                        de.dpdc = True
                        de.has_critical_alarm = True
                    elif de.dpd <= als['alarm_dp_diff']:
                        de.dpda = True
                        de.has_alarm = True

            if de.wt >= self.config['mins']['wtf']:
                if als['monitor_wt']:
                    if als['critical_wt_h'] <= de.wt or de.wt <= als['critical_wt_l']:
                        de.wtc = True
                        de.has_critical_alarm = True
                    elif als['alarm_wt_h'] <= de.wt or de.wt <= als['alarm_wt_l']:
                        de.wta = True
                        de.has_alarm = True

            if de.wf >= self.config['mins']['wf']:
                if als['monitor_wf']:
                    if als['critical_wf_h'] <= de.wf or de.wf <= als['critical_wf_l']:
                        de.wfc = True
                        de.has_critical_alarm = True
                    elif als['alarm_wf_h'] <= de.wf or de.wf <= als['alarm_wf_l']:
                        de.wfa = True
                        de.has_alarm = True

            if de.vp >= self.config['mins']['vp']:
                if als['monitor_vp']:
                    if als['critical_vp_h'] <= de.vp or de.vp <= als['critical_vp_l']:
                        de.vpc = True
                        de.has_critical_alarm = True
                    elif als['alarm_vp_h'] <= de.vp or de.vp <= als['alarm_vp_l']:
                        de.vpa = True
                        de.has_alarm = True

            if de.he == self.config['he_noread']:
                if als['monitor_he']:
                    if de.he <= als['critical_he_l']:
                        de.hec = True
                        de.has_critical_alarm = True
                    elif de.he <= als['alarm_he_l']:
                        de.hea = True
                        de.has_alarm = True

            if de.vp >= self.config['mins']['vp']:
                if als['monitor_vp'] and als['in_storage']:
                    if als['critical_storage_vp_h'] <= de.vp or de.vp <= als['critical_storage_vp_l']:
                        de.vpc = True
                        de.has_critical_alarm = True
                    elif als['alarm_storage_vp_h'] <= de.vp or de.vp <= als['alarm_storage_vp_l']:
                        de.vpa = True
                        de.has_alarm = True

            if de.he == self.config['he_noread']:
                if als['monitor_he'] and als['in_storage']:
                    if de.he <= als['critical_storage_he_l']:
                        de.hec = True
                        de.has_critical_alarm = True
                    elif de.he <= als['alarm_storage_he_l']:
                        de.hea = True
                        de.has_alarm = True

            if als['monitor_cmp']:
                if de.cmp is False:
                    de.cmpc = True
                    de.has_critical_alarm = True

            if als['monitor_q'] and als['in_storage'] is False:
                if de.q is False:
                    de.qc = True
                    de.has_critical_alarm = True
        except Exception as e:
            self.log.error(e)
            self.config['errors'][datetime.now().strftime(self.config['oiv_dt'])] = "RDC Check Data Error: {}".format(e)
            return False

        self.log.info('Data Processed: {}'.format(vars(de)))
        return de

    def saveData(self, de: dataEntity):
        self.log.info("Saving Data")

        sql = "INSERT INTO data (mac, tf, rh, dp, dpd, he, wf, wt, vp, q, cmp, in_storage, has_alarm, has_critical_alarm, tfa, rha, dpa, dpda, hea, vpa, wfa, wta, tfc, rhc, dpc, dpdc, hec, vpc, wfc, wtc, qc, cmpc, uploaded, `timestamp`) VALUES ('{0}',{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},'{33}');".format(
            de.mac,
            de.tf,
            de.rh,
            de.dp,
            de.dpd,
            de.he,
            de.wf,
            de.wt,
            de.vp,
            int(de.q),
            int(de.cmp),
            int(de.in_storage),
            int(de.has_alarm),
            int(de.has_critical_alarm),
            int(de.tfa),
            int(de.rha),
            int(de.dpa),
            int(de.dpda),
            int(de.hea),
            int(de.vpa),
            int(de.wfa),
            int(de.wta),
            int(de.tfc),
            int(de.rhc),
            int(de.dpc),
            int(de.dpdc),
            int(de.hec),
            int(de.vpc),
            int(de.wfc),
            int(de.wtc),
            int(de.qc),
            int(de.cmpc),
            int(de.uploaded),
            self.config['box']['date']
        )

        if self.conn is None:
            return False
        try:
            c = self.conn.cursor()
            c.execute(sql)
            self.conn.commit()
        except sqlite3.DatabaseError as de:
            pprint(de)
            self.log.error(de)
            self.config['errors'][datetime.now().strftime(self.config['oiv_dt'])] = "RDC Save Data Error: {}".format(de)
            cfg = oivConfig()
            cfg.connectDb(True)
            return False
        except Exception as e:
            pprint(e)
            self.log.error(e)
            self.config['errors'][datetime.now().strftime(self.config['oiv_dt'])] = "RDC Save Data Error: {}".format(e)
            return False
        return True

    def uploadData(self):
        self.log.info("Uploading Data")
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        js = None
        readings = []

        self.conn.row_factory = sqlite3.Row
        c = self.conn.cursor()
        c.execute('SELECT * FROM data WHERE uploaded = 0 ORDER BY id ASC LIMIT {};'.format(self.config['uploadMax']))
        t = c.fetchall()  # type: sqlite3.Row
        rowIds = []
        for row in t:
            rowIds.append(row['id'])
            tmp = {
                'tf': row['tf'],
                'rh': row['rh'],
                'dp': row['dp'],
                'dpd': row['dpd'],
                'he': row['he'],
                'vp': row['vp'],
                'wf': row['wf'],
                'wt': row['wt'],
                'q': row['q'],
                'cmp': row['cmp'],
                'timestamp': row['timestamp']
            }
            readings.append(tmp)

        uploaded = False
        for server in self.config['server']:
            try:
                headers = {'Content-Type': 'application/json',
                           'api-key': self.config['key']}
                data = {'macrdc': self.config['mac_rdc'],
                        'macoiv': self.config['mac_oiv'],
                        'mfg': self.config['mfg'],
                        'data': readings}
                if self.debug:
                    print("Data to send for upload:")
                    pprint(data)
                req = requests
                res = req.request('POST', "{}/api/data/ge".format(server), verify=False, timeout=60, headers=headers, allow_redirects=True, data=json.dumps(data))
                if self.debug:
                    print("Request Response:")
                    pprint(res.text)
                try:
                    js = json.loads(res.text)
                    self.goodServer(server=server)
                    if js['code'] is not 202:
                        self.log.error('API code: {0} Error: {1}'.format(js['code'], js['error']))
                        exit(3)
                        return False
                except ValueError:
                    exit(4)
                    return False

                uploaded = True
                break

            except requests.ConnectionError as ce:
                pprint(ce)
                continue

        if uploaded is False:
            self.log.warn("Upload failed")
            return False

        try:
            sRowIds = [str(a) for a in rowIds]
            c = self.conn.cursor()
            c.execute('''UPDATE data SET uploaded = 1 WHERE id IN ({})'''.format(", ".join(sRowIds)))
            self.conn.commit()
        except Exception as e:
            self.log.error(e)


        self.config['upload'] = js
        self.config['alarmSettings'] = self.config['upload']['settings']
        self.saveAlarmSettings()
        self.goodUpload()
        self.setBoxSiteInfo(upload=True)
        self.setBoxMonitorInfo()
        if 'new_token' in self.config['upload']:
            cfg = oivConfig()
            cfg.setKey(self.config['upload']['new_token'])
        return True

    def dbCleanUp(self):
        try:
            c = self.conn.cursor()
            c.execute('SELECT `timestamp`, last_clean FROM cleaning ORDER BY id DESC LIMIT 1;')
            t = c.fetchone()
            last_clean = datetime.strptime(t[0], self.config['sql_dt'])
        except TypeError as te:
            self.log.error("1 Last clean error: {}".format(te))
            self.config['errors'][datetime.now().strftime(self.config['oiv_dt'])] = "1 Last clean error: {}".format(te)
            last_clean = None
        except sqlite3.DatabaseError as de:
            self.log.error("2 Last Clean error: {}".format(de))
            self.config['errors'][datetime.now().strftime(self.config['oiv_dt'])] = "2 Last clean error: {}".format(de)
            last_clean = None

        if last_clean is None:
            self.goodClean()
            return None

        now = datetime.now() - timedelta(days=self.config['cleanDays'])

        if now < last_clean:
            return None

        now = datetime.now() - timedelta(days=self.config['dataKeep'])
        self.log.info("Cleaning 'data' prior to {}".format(now.isoformat()))
        try:
            c = self.conn.cursor()
            c.execute(
                'DELETE FROM data WHERE CAST(strftime("%s", timestamp) AS integer) <= CAST(strftime("%s", "{}") AS integer) ;'.format(now.isoformat()))
            self.conn.commit()
        except Exception as e:
            self.log.error("Clean data error: {}".format(e))
            pprint(e)

        now = datetime.now() - timedelta(days=self.config['helloKeep'])
        self.log.info("Cleaning 'hello' prior to {}".format(now.isoformat()))
        try:
            self.conn.row_factory = sqlite3.Row
            c = self.conn.cursor()
            c.execute(
                'DELETE FROM hellos WHERE CAST(strftime("%s", timestamp) AS integer) <= CAST(strftime("%s", "{}") AS integer) ;'.format(now.isoformat()))
            self.conn.commit()
        except Exception as e:
            self.log.error("Clean hello error: {}".format(e))
            pprint(e)

        now = datetime.now() - timedelta(days=self.config['uploadKeep'])
        self.log.info("Cleaning 'upload' prior to {}".format(now.isoformat()))
        try:
            self.conn.row_factory = sqlite3.Row
            c = self.conn.cursor()
            c.execute(
                'DELETE FROM uploads WHERE CAST(strftime("%s", timestamp) AS integer) <= CAST(strftime("%s", "{}") AS integer) ;'.format(now.isoformat()))
            self.conn.commit()
        except Exception as e:
            self.log.error("Clean upload error: {}".format(e))
            pprint(e)

        now = datetime.now() - timedelta(days=self.config['cleanKeep'])
        self.log.info("Cleaning 'cleaning' prior to {}".format(now.isoformat()))
        try:
            self.conn.row_factory = sqlite3.Row
            c = self.conn.cursor()
            c.execute(
                'DELETE FROM cleaning WHERE CAST(strftime("%s", timestamp) AS integer) <= CAST(strftime("%s", "{}") AS integer) ;'.format(now.isoformat()))
            self.conn.commit()
        except Exception as e:
            self.log.error("Clean cleaning error: {}".format(e))
            pprint(e)

        self.goodClean()

    def checkVers(self, minVer = "0.0.0.0", boxVer="0.0.0.0"):
        mv = minVer.split('.')
        bv = boxVer.split('.')

        if mv[0] > bv[0]:
            return False

        if mv[1] > bv[1]:
            return False

        if mv[2] > bv[2]:
            return False

        if mv[3] > bv[3]:
            return False

        return True

    def checkRevs(self, minRev = "0.0", boxRev="0.0"):
        mr = minRev.split('.')
        br = boxRev.split('.')

        if mr[0] > br[0]:
            return False

        if mr[1] > br[1]:
            return False

        return True

    def test(self):
        pprint(self.config)