#!/usr/bin/python3
import sqlite3
from oiv import oivUtils, oivConfig
import logging
from logging import handlers
import sys
import json
from datetime import datetime, timezone, timedelta
from pprint import pprint
import argparse
from time import sleep
import re, os

debug = False
comm = None
config = None
daemon = False
forceHello = False
forceUpload = False
root = None
reset = False

def setupLogging():
    global log

    log.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(formatter)
    if debug or not daemon:
        log.addHandler(ch)

    fh = handlers.RotatingFileHandler('{}var/oivision.log'.format(root), maxBytes=(1048576 * 5), backupCount=4)
    fh.setFormatter(formatter)
    log.addHandler(fh)

def initBox(reboot=False):
    if reboot:
        log.info("Rebooting box")
        oivutils.sendCmd('r')
        sleep(30)
    log.info("Initializing the box")
    oivutils.sendCmd(cmd='x')
    oivutils.sendJson(cmd='&t', data={'date': datetime.now(tz=timezone.utc).__format__('%Y-%m-%dT%H:%M:%S')})
    oivutils.setBoxOffsetData()
    config['box'] = oivutils.getOivInfo(conn)
    config['offsets'] = oivutils.getJson('o')
    config['vessel'] = oivutils.getJson('v')

def getScriptDirectory():
    path = os.path.realpath(sys.argv[0])
    if os.path.isdir(path):
        return path+os.sep
    else:
        return os.path.dirname(path)+os.sep

if __name__ == '__main__':
    root = getScriptDirectory()
    parser = argparse.ArgumentParser(prog='oivision', usage='%(prog)s [options]')
    parser.add_argument('-d', '--daemon', action='store_true', help='Run script in daemon mode')
    parser.add_argument('-f', '--force', action='store_true', help='Force hello and upload')
    parser.add_argument('-v', '--verbose', action='store_true', help='Verbose output')
    parser.add_argument('-r', '--reset', action='store_true', help='Reset to factory')
    parser.add_argument('-s', '--setup', action='store_true', help='Setup connected box')
    args = parser.parse_args()
    daemon = args.daemon
    force = args.force
    debug = args.verbose
    reset = args.reset
    setupBox = args.setup

    if force:
        forceHello = True
        forceUpload = True

    log = logging.getLogger(__name__)
    setupLogging()

    oivcfg = oivConfig(log=log, debug=debug, root=root)
    config = oivcfg.getConfig()

    if reset:
        oivcfg.reset()
        setupLogging()
        log.warning("Factory Reset")
        print("Rerun {}".format(__file__))
        exit(0)

    conn = oivcfg.connectDb()

    oivutils = oivUtils(sqliteconn=oivcfg.connectDb(), log=log, debug=debug, config=config)
    config['key'] = oivcfg.getKey()
    log.info("Using API Key: {}".format(config['key']))

    config['mac_rdc'] = oivutils.getRdcInfo(config['ether']).lower()
    config['root'] = root

    #
    # Get the last server that we successfully sent data to
    #
    try:
        c = conn.cursor()
        c.execute('SELECT last_host FROM server ORDER BY id DESC LIMIT 1;')
        t = c.fetchone()
        config['server'].insert(0, t[0])
    except TypeError as te:
        log.error("Get last server error: {}".format(te))
        # config['errors'][datetime.now().strftime(config['oiv_dt'])] = "RDC Get last server error: {}".format(te)

    #
    # Connect the serial port
    #
    comm = oivutils.getSerialPorts()
    config['comm'] = comm
    if comm is None:
        print("No Comm Port. Exiting!")
        log.error("No comm port found. Exiting!")
        exit(1)
    log.info('Using port: {}'.format(comm))

    oivutils.startSerial(port=comm, baud=config['baud'])
    if not oivutils.isBoxConnected(True):
        print("No oiv box connected. Exiting!")
        log.error("No oiv box connected. Exiting!")
        exit(1)

    #
    # Setup box. Resets box to factory and sets revision
    #
    if setupBox:
        log.info("Setting up box")
        revChoices = ['1.7', '1.6', '1.5', '1.4', '7', '6', '5', '4']
        while True:
            rev = input("Enter the connected box revision (example 1.7 or 7): ")
            if rev in revChoices:
                break
            else:
                print("Incorrect format for {} please type 1.4 1.5 1.6 or 1.7 or just 4 5 6 or 7".format(rev))
        if float(rev) > 3:
            rev = str((float(rev) * 0.1) + 1)

        print("Setting up box as revision {}".format(rev))
        oivutils.sendCmd(cmd='&f')
        sleep(2)
        oivutils.sendJson(cmd='&i', data={'rev':rev})
        sleep(2)

    #
    # Get the oiv box info
    #
    try:
        config['box'] = oivutils.getOivInfo(conn)
    except TypeError as te:
        print("Could not communicate with the box. Exiting!")
        log.error("Could not communicate with the box. Exiting!")
        exit(1)

    config['mac_oiv'] = config['box']['mac']

    try:
        with open("{}/oivmac".format(config['rdcdir']), "w") as oivmacf:
            oivmacf.write(config['mac_oiv'])
        os.chmod("{}/oivmac".format(config['rdcdir']), 0o666)
    except Exception as e:
        log.error(e)

    if setupBox:
        pprint(config['box'])
        print("Box setup complete. Exiting.")
        exit(0)

    #
    # Check the box version and revision
    #
    if not oivutils.checkVers(config['oivMinVer'], config['box']['version']):
        print("Box version does not meet minimum requirements. Minimum version: {}  Box version: {}".format(config['oivMinVer'], config['box']['version']))
        log.error("Box version does not meet minimum requirements. Minimum version: {}  Box version: {}".format(config['oivMinVer'], config['box']['version']))
        exit(1)

    if not oivutils.checkRevs(config['oivMinRev'], config['box']['revision']):
        print("Box revision does not meet minimum requirements. Minimum revision: {}  Box Version: {}".format(config['oivMinRev'], config['box']['revision']))
        log.error("Box revision does not meet minimum requirements. Minimum revision: {}  Box Version: {}".format(config['oivMinRev'], config['box']['revision']))
        exit(1)

    #
    # Load the last alarm settings
    #
    oivutils.loadAlarmSettings()

    #
    # Initialize the box if the date and time is off more that X seconds from RDC
    #
    try:
       bdt = config['box']['date']
       bdt = datetime.strptime(bdt, config['oiv_dt'])  # type: datetime
       deltaT = datetime.now() - bdt
       deltaTsecs = deltaT.seconds + (deltaT.days * 86400)
       if deltaTsecs > config['deltaT']:
           initBox(reboot=False)
           forceHello = True
           config['errors'] = oivutils.getJson('e')
           config['data'] = oivutils.getJson('d')
    except Exception as e:
        print(e)
        config['errors'][datetime.now().strftime(config['oiv_dt'])] = "RDC Init Box error: {}".format(e)

    #
    # Get the errors and data from the box
    #
    config['errors'] = oivutils.getJson('e')
    config['data'] = oivutils.getJson('d')

    #
    # Check if it is time to reboot the box
    #
    days = '0'
    regex = re.compile('\d{1,2}\.\d{2}:')
    p = regex.match(config['box']['uptime'])
    if p is not None:
        # days exist
        days = config['box']['uptime'].split('.', 1)[0]
        hms = config['box']['uptime'].split('.', 1)[1].split(':', 2)
    else:
        hms = config['box']['uptime'].split(':', 2)
    deltaR = timedelta(days=float(days), hours=float(hms[0]), minutes=float(hms[1]), seconds=float(hms[2]))

    deltaRhours = (deltaR.days * 3600) + (deltaR.seconds / 60 / 60)
    if deltaRhours >= config['deltaR']:
        initBox(reboot=True)
        forceHello = True
        config['errors'] = oivutils.getJson('e')
        config['data'] = oivutils.getJson('d')
    #
    # Check if it is time to say HELLO again
    #
    try:
        c = conn.cursor()
        c.execute('SELECT `timestamp`, last_hello FROM hellos ORDER BY id DESC LIMIT 1;')
        t = c.fetchone()
        last_hello = t[0]
        config['hello'] = json.loads(t[1])
    except TypeError as te:
        log.error("1 Get last hello error: {}".format(te))
        config['errors'][datetime.now().strftime(config['oiv_dt'])] = "1 RDC Get last hello error: {}".format(te)
        last_hello = None
    except sqlite3.DatabaseError as de:
        log.error("2 Get last hello error: {}".format(de))
        config['errors'][datetime.now().strftime(config['oiv_dt'])] = "2 RDC Get last hello error: {}".format(de)
        last_hello = None
        oivcfg.connectDb(True)

    helloRet = False
    if last_hello is None:
        helloRet = oivutils.sayHello()
    elif forceHello:
        helloRet = oivutils.sayHello()
    else:
        lhdt = datetime.strptime(last_hello, config['sql_dt'])
        deltaH = datetime.now() - lhdt
        deltaHhours = (deltaH.seconds / 60 / 60) + (deltaH.days * 24)
        if deltaHhours >= config['deltaH']:
            helloRet = oivutils.sayHello()

    if helloRet is False and 'hello' not in config:
        print('Hello returned with an error. Exiting!')
        log.error('Hello returned with an error. Exiting!')
        exit(1)

    oivutils.loadAlarmSettings()

    #
    # Check the data for alarms
    #
    de = oivutils.checkData()
    if de is False:
        log.error('Checking/Storing the data failed. Exiting!')
        exit(1)

    #
    # Check if need to upload
    #
    try:
        c = conn.cursor()
        c.execute('SELECT `timestamp` FROM uploads ORDER BY id DESC LIMIT 1;')
        t = c.fetchone()
        last_upload = datetime.strptime(t[0], config['sql_dt'])
    except TypeError as te:
        log.error("Get last upload error: {}".format(te))
        config['errors'][datetime.now().strftime(config['oiv_dt'])] = "RDC Get last upload error: {}".format(te)
        last_upload = datetime.strptime('2000-01-01', '%Y-%m-%d')

    try:
        c = conn.cursor()
        c.execute('SELECT COUNT(*) FROM data WHERE uploaded = 0 ORDER BY id ASC LIMIT {};'.format(config['uploadMax']))
        t = c.fetchone()
        if int(t[0]) >= 2:
            last_upload = datetime.strptime('2000-01-01', '%Y-%m-%d')
            log.info('Found backlog of data. Uploading to run.')
    except Exception as e:
        log.error(e)


    if de.has_critical_alarm:
        log.warning("Critical Alarm found in data")
        if oivutils.saveData(de) is False:
            log.error("Saving critical alarm data failed")
        now = datetime.now() - timedelta(minutes=config['critical_send_freq'])
    elif de.has_alarm:
        log.warning("Alarm found in data")
        if oivutils.saveData(de) is False:
            log.error("Saving alarm data failed")
        now = datetime.now() - timedelta(minutes=config['alarm_send_freq'])
    else:
        log.info("Data is nominal")
        now = datetime.now() - timedelta(minutes=config['nominal_send_freq'])
        if now >= last_upload:
            if oivutils.saveData(de) is False:
                log.error("Saving nominal data failed")

    if forceUpload:
        if oivutils.saveData(de) is False:
            log.error("Saving alarm data failed")

    if now >= last_upload or forceUpload:
        oivutils.uploadData()

    #
    # Set error messages
    #
    oivutils.setBoxErrMsg(de)


    #
    # Close the serial port
    #
    oivutils.stopSerial()

    #
    # If errors say hello with the errors
    #
    if len(config['errors']) > 0:
        log.info('Do HELLO because errors were found')
        if debug:
            pprint("DEBUG - Errors: {}".format(config['errors']))
        oivutils.sayHello()

    #
    # Clean up
    #
    oivutils.dbCleanUp()

    #
    # Done
    #
    log.info('DONE')

    # oivutils.test()

    exit(0)



