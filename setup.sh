#!/bin/bash
#Check if root
if [ "$EUID" -ne 0 ]; then
	echo "Please run setup.sh as root"
	exit 1
fi
cd /usr/local/oivision
chmod 775 oivision-update
chmod 775 oivision.py
chmod 775 var
chmod 775 oiv
./oivision.py -r

echo "OiVision Install Completed"

exit 0
